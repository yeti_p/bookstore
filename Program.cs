﻿using System;
using System.Linq;

namespace BookStores
{
    class Program
    {
        private InOut _inOut;

        public Program()
        {
            DataBooks.CreateTestData();
            _inOut = new InOut();
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            while (true)
            {

                Console.WriteLine("Available commands:");
                Console.WriteLine("1. Add Author");
                Console.WriteLine("2. Add Book");
                Console.WriteLine("3. Print all books");
                Console.WriteLine("4. Print all books by author surname");
                Console.WriteLine("5. Sell book");
                Console.WriteLine("6. Print sold books");
                Console.WriteLine("7. Exit");

                var input = _inOut.GetUintFromUser("Enter command number");

                Console.Clear();

                switch (input)
                {
                    case 1:
                        AddAuthor();
                        CleanScreen();
                        break;
                    case 2:
                        AddBook();
                        CleanScreen();
                        break;
                    case 3:
                        AllBooks();
                        CleanScreen();
                        break;
                    case 4:
                        BooksByAuthorSurname();
                        CleanScreen();
                        break;
                    case 5:
                        SellBook();
                        CleanScreen();
                        break;
                    case 6:
                        PrintSoldBooks();
                        CleanScreen();
                        break;
                    case 7:
                        Exit();
                        return;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Unknown command");
                        Console.ResetColor();
                        Console.WriteLine();
                        break;
                };
            }
        }

        private void Exit()
        {
            Environment.Exit(0);
        }

        private void PrintSoldBooks()
        {
            if (DataBooks.BooksSold.Count() > 0)
            {
                foreach (var book in DataBooks.BooksSold)
                {
                    double ParsedBookPrice = 0;
                    double.TryParse(book.BookPrice, out ParsedBookPrice);
                    var TotalRevenue = book.Sold * ParsedBookPrice;
                    Console.WriteLine($"Author: {book.Author}," +
                        $" Title: {book.Title}," +
                        $" Book Sold: {book.Sold}," +
                        $" Total revenue: {TotalRevenue} zl");
                }

            }
            else
            {
                Console.WriteLine("No books to print!");
            }
        }

        private void SellBook()
        {
            var Title = _inOut.GetStringFromUser("Insert title of book to sell: ");
            TitleSellBooks(Title);
        }

        private void TitleSellBooks(string title)
        {
            var NumberOfBooks = DataBooks.Books.Count();
            var Counter = 0;
            foreach (var book in DataBooks.Books.ToList())
            {
                if (book.Title.Contains(title) && book.Quantity > 0)
                {
                    Console.WriteLine($"This book is sold: {book.Title}");
                    book.Quantity--;
                    if (DataBooks.BooksSold.Contains(book))
                    {
                        book.Sold++;
                    }
                    else
                    {
                        DataBooks.BooksSold.Add(book);
                        book.Sold++;
                    }


                }
                else
                {
                    Counter++; //licznik sprawdzajcy czy chociaz jeden alement z listy jest tym ktorego szukam
                }

            }
            if (Counter == NumberOfBooks)
            {
                Console.WriteLine("no such book was found!");
            }
        }

        private void BooksByAuthorSurname()
        {
            var AuthSurname = _inOut.GetStringFromUser("Show surname author of book");
            AllBooksAuth(AuthSurname);
        }

        private void AllBooks()
        {
            if (DataBooks.Books.Count() > 0)
            {
                Console.WriteLine("List of books: ");
                foreach (var book in DataBooks.Books)
                {
                    Console.WriteLine($"Author: { book.Author}," +
                        $" title: {book.Title}," +
                        $" year of publish: {book.YearOfPublishment}," +
                        $" book price: {book.BookPrice} zl");
                }
            }
            else
            {
                Console.WriteLine("All books is sold!");
            }
        }


        private void AllBooksAuth(string AuthSurname)
        {
            foreach (var book in DataBooks.Books)
            {
                if (book.Author.Contains(AuthSurname))
                {

                    Console.WriteLine($"Author: { book.Author}," +
                    $" title: {book.Title}," +
                    $" year of publish: {book.YearOfPublishment}," +
                    $" book price: {book.BookPrice} zl");
                }
            }
        }

        private void AddAuthor()
        {
            Console.WriteLine("Adding new author. Please enter following data:");
            var newAuthor = new Author
            {
                Name = _inOut.GetStringFromUser("Name"),
                Surname = _inOut.GetStringFromUser("Surname"),
                YearOfBirth = _inOut.GetYearFromUser("Year of birth")
            };

            DataBooks.Authors.Add(newAuthor);
            Console.WriteLine($"Author added. ID = {newAuthor.ID}");
        }

        private void AddBook()
        {
            Console.WriteLine("Adding new book. Please enter following data:");
            var newBook = new Book
            {
                Title = _inOut.GetStringFromUser("Title"),
                YearOfPublishment = _inOut.GetYearFromUser("YearOfPublishment"),
                Author = _inOut.GetStringFromUser("Author"),
                Quantity = _inOut.GetUintFromUser("Quantity"),
                BookPrice = String.Format("{0:0.00}", _inOut.GetDoubleFromUser("BookPrice"))
            };

            DataBooks.Books.Add(newBook);
            Console.WriteLine($"Book added. ID = {newBook.ID}");
        }
        private void CleanScreen()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
