﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStores
{
    internal static class DataBooks
    {
        public static List<Book> Books = new List<Book>();
        public static List<Book> BooksSold = new List<Book>();
        public static List<Author> Authors = new List<Author>();
        public static List<DataSellBooks> DataSellBook = new List<DataSellBooks>();
        //public static List<DataAuthBook> DataAuthBooks= new List<DataAuthBook>();
        //public static List<Rental> Rentals = new List<Rental>();

        public static void CreateTestData()
        {
            var behawiorysta = new Book()
            {
                Title = "Behawiorysta",
                YearOfPublishment = 2016,
                Author = "Remigiusz Mroz",
                BookPrice = "20,66",
                Quantity = 2,
                Sold = 0
                //IsAvailable = true,
            };

            var ekspozycja = new Book()
            {
                Title = "Ekspozycja",
                YearOfPublishment = 2015,
                Author = "Remigiusz Mroz",
                BookPrice = "22,84",
                Quantity = 18,
                Sold = 0
                //IsAvailable = true,
            };

            var florystka = new Book()
            {
                Title = "Florystka",
                YearOfPublishment = 2012,
                Author = "Katarzyna Bonda",
                BookPrice = "10,43",
                Quantity = 5,
                Sold = 0
                //IsAvailable = true,
            };

            var mroz = new Author()
            {
                Name = "Remigiusz",
                Surname = "Mroz",
                YearOfBirth = 1987
            };

            var bonda = new Author()
            {
                Name = "Katarzyna",
                Surname = "Bonda",
                YearOfBirth = 1977
            };

            Books.Add(behawiorysta);
            Books.Add(ekspozycja);
            Books.Add(florystka);

            Authors.Add(mroz);
            Authors.Add(bonda);
            //Cars.Add(Bonda);

            //DataSellBook.Add(new DataSellBooks { book = florystka, Author = bonda, BookPrice = 10.43 });
            //Rentals.Add(new Rental { Car = legacy, User = hannah, IsActive = false, Distance = 13.1 });
            //Rentals.Add(new Rental { Car = legacy, User = tom, IsActive = false, Distance = 34.7 });
        }

    }
}