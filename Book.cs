﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStores
{
    internal class Book
    {
        private static uint _lastId = 0;

        public uint ID { get; private set; } = ++_lastId;
        public string Title;
        public uint YearOfPublishment;
        //public string Author;
        public Author Author;
        public string BookPrice;
        public uint Quantity;
        public uint Sold = 0;
    }
}
