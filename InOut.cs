﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStores
{
    class InOut
    {
        public uint GetUintFromUser(string message)
        {
            uint result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = uint.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number. Try again...");
                }
            }

            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number");
                }
            }

            return result;
        }

        public bool GetBoolFromUser(string message)
        {
            bool result = false;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = bool.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a true or false. Try again...");
                }
            }

            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            var author = Console.ReadLine();
            return author;
        }

        public uint GetYearFromUser(string message)
        {
            var success = false;
            uint result = 0;
            string input;

            while (!success)
            {
                Console.Write($"{message}: ");
                input = Console.ReadLine();
                if (input.Length == 4)
                {
                    success = uint.TryParse(input, out result);
                    if(!success)
                    {
                        Console.WriteLine("Wrong number! Try again");
                    }
                }
                else
                {
                    Console.WriteLine("Wrong number! Try again");
                }
            }
            return result;
        }

    }
}
