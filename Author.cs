﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStores
{
    class Author
    {
        private static uint _lastId = 0;

        public uint ID { get; private set; } = ++_lastId;
        public string Name;
        public string Surname;
        public uint YearOfBirth;
    }
}
